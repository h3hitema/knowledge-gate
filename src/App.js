import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './pages/Home';
import Users from './pages/Admin/Users/Users';
import Login from './pages/Login';
import DetailsUser from './pages/Admin/Users/DetailsUser';
import Header from './components/Header';
import Register from './pages/Register';
import EditUser from "./pages/Admin/Users/EditUser";
import CreateUser from "./pages/Admin/Users/CreateUser";
import Disciplines from "./pages/Admin/Disciplines/Disciplines";
import CreateDiscipline from "./pages/Admin/Disciplines/CreateDiscipline";
import DetailsDiscipline from "./pages/Admin/Disciplines/DetailsDiscipline";
import EditDiscipline from "./pages/Admin/Disciplines/EditDiscipline";
import TeacherDisciplines from "./pages/Teacher/Disciplines/TeacherDisciplines";
import TeacherDetailsDiscipline from "./pages/Teacher/Disciplines/TeacherDetailsDiscipline";
import TeacherShedules from "./pages/Teacher/Disciplines/TeacherShedules";
import StudentDisciplines from "./pages/Student/StudentDisciplines";
import StudentDetailsDiscipline from "./pages/Student/StudentDetailsDiscipline";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Header/>
                {/* Admin Users*/}
                <Route exact path='/admin/users' component={Users}/>
                <Route exact path='/admin/users/create' component={CreateUser}/>
                <Route exact path='/admin/users/:id([0-9]+)' component={DetailsUser}/>
                <Route exact path='/admin/users/edit/:id' component={EditUser}/>

                <Route exact path='/admin/disciplines' component={Disciplines}/>
                <Route exact path='/admin/disciplines/create' component={CreateDiscipline}/>
                <Route exact path='/admin/disciplines/:id([0-9]+)' component={DetailsDiscipline}/>
                <Route exact path='/admin/disciplines/edit/:id' component={EditDiscipline}/>

                {/* Teacher Users*/}
                <Route exact path='/teacher/disciplines' component={TeacherDisciplines}/>
                <Route exact path='/teacher/disciplines/:id([0-9]+)' component={TeacherDetailsDiscipline}/>
                <Route exact path='/teacher/schedule' component={TeacherShedules}/>

                {/* Teacher Users*/}
                <Route exact path='/student/disciplines' component={StudentDisciplines}/>
                <Route exact path='/student/disciplines/:id([0-9]+)' component={StudentDetailsDiscipline}/>

                {/* Public Routes*/}
                <Route exact path='/' component={Home}/>
                <Route exact path='/login' component={Login}/>
                <Route exact path='/register' component={Register}/>
            </BrowserRouter>
        )
    }
}

export default App;
