import React, {Component} from 'react';
import StudentDiscipline from "../../components/StudentDiscipline";
import DisciplineService from "../../services/DisciplineService";

class StudentDisciplines extends Component{

    state = {
        disciplines: []
    };

    /**
     * Récupération de toutes les disciplines
     * @returns {Promise<void>}
     */
    componentDidMount = async () => {
        let response = await DisciplineService.list();
        console.log(response);
        if (response.ok) {
            let data = await response.json();
            console.log(data);
            this.setState({disciplines: data.disciplines})
        }
    };

    render(){
        return(
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Slug</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.disciplines.map(item => {
                                return (
                                    <StudentDiscipline  data={item} />
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default StudentDisciplines;
