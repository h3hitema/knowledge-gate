import React, {Component} from 'react';
import DisciplineService from "../../services/DisciplineService";
import  {Calendar, momentLocalizer} from 'react-big-calendar' ;
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import CreateCandidature from "../../components/CreateCandidature";

class StudentDetailsDiscipline extends Component {

    state = {
        discipline: {
            teachers: [],
            classrooms: []
        },
        localizer: momentLocalizer(moment),
        events: []

    };

    componentDidMount = async () => {
        /**
         * Discipline with relations
         */
        let {id} = this.props.match.params;
        let response = await DisciplineService.details(id);
        if (response.ok) {
            let data = await response.json();
            console.log(data);
            this.setState({discipline: data.discipline})
        }

        /**
         * Calendar
         */
        const events = this.state.discipline.classrooms.map((classroom) => {
            return {
                title: `Cours de ${this.state.discipline.name}`,
                allDay: false,
                start: new Date(classroom.dateStart),
                end: new Date(classroom.dateENd),
            }
        });
        moment.locale('fr-FR');
        const localizer = await momentLocalizer(moment);
        this.setState({localizer: localizer, events: events});
    };

    async delete(id) {
        const response = await DisciplineService.delete(id);

        if (response.ok) {
            this.props.history.push('/admin/disciplines');
        }
    }

    render() {
        let {id, name, slug, description} = this.state.discipline;
        let localizer = this.state.localizer;

        return (
            <div className="container">
                <h1>Détails de la discipline...</h1>
                <p>Nom: {name}</p>
                <p>Slug: {slug}</p>
                <p>Description: {description}</p>
                <div>
                    Professeurs:
                    <ul>
                        {
                            this.state.discipline.teachers.map(item => {
                                return (
                                    <li key={item.id}>{item.firstname} {item.lastname}</li>
                                )
                            })
                        }
                    </ul>
                </div>
                <CreateCandidature id={id}/>
                <div style={{height:600+'px'}} className="calendar">
                    <Calendar
                        events={this.state.events}
                        localizer={localizer}
                        startAccessor={"start"}
                        endAccessor={"end"}
                    />
                </div>
            </div>
        )
    }
}

export default StudentDetailsDiscipline;
