import React, { Component } from 'react';
import UserService from '../services/UserService';

class Login extends Component {

    state = {
        email: "",
        password: ""
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    async submit(e) {
        e.preventDefault();
        let response = await UserService.auth(this.state)
        if (response.ok) {
            let data = await response.json();
            console.log(data);

            localStorage.setItem('token', data.token);
            localStorage.setItem('user', JSON.stringify(data.user));
            this.props.history.push('/');
        } else {
            //.....
            console.log('get out')
        }
    }

    render() {
        return (
            <div className="conteiner">
                <form onSubmit={(e) => this.submit(e)} >
                    <div className="form-group">
                        <label htmlFor="">Email</label>
                        <input id="username" className="form-control" onChange={(e) => this.handleChange(e)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Password</label>
                        <input type="password" id="password" className="form-control" onChange={(e) => this.handleChange(e)} required />
                    </div>
                    <input className="btn btn-primary" type="submit" value="Connexion" />
                </form>
            </div>
        )
    }
}
export default Login;