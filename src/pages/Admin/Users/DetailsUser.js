import React, { Component } from 'react';
import UserService from "../../../services/UserService";
import {Link} from "react-router-dom";

class DetailsUser extends Component {

    state = {
        user: {}
    };

    /**
     * Récupération desinformation de l'utilisateur
     * @returns {Promise<void>}
     */
    componentDidMount = async () => {
        let { id } = this.props.match.params;

        let response = await UserService.details(id);
        if (response.ok) {
            let data = await response.json();
            console.log(data);
            this.setState({user: data.user})
        }
    };

    /**
     * Appelé au click du button pour supprimé l'utilisateur
     * @param id
     * @returns {Promise<void>}
     */
    async delete(id) {
        const response = await UserService.delete(id);
        if(response.ok){
            this.props.history.push('/admin/users');
        }
    }

    render() {
        let {id, firstname, lastname, email, username, role} = this.state.user;
        return (
            <div className="container">
                <h1>Détails de l'utilisateur...</h1>
                <p>Username: {username}</p>
                <p>Email: {email}</p>
                <p>Firstname: {firstname}</p>
                <p>Lastname: {lastname}</p>
                <p>Role: {role}</p>
                <p>
                    <button className="btn btn-danger" onClick={() => this.delete(id) }>Delete</button>
                </p>
                <p>
                    <Link className="btn btn-warning" to={`/admin/users/edit/${id}`} >Editer</Link>
                </p>
            </div>
        )
    }
}

export default DetailsUser;
