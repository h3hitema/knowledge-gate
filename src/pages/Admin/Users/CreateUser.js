import React, {Component} from 'react';
import UserService from "../../../services/UserService";

class CreateUser extends Component {

    state = {
        user: {
            firstname: '',
            lastname: '',
            email: '',
            role: '',
            username: '',
            password: ''
        }
    };

    async componentDidMount() {

    }

    /**
     * Récupération des donnés du formulaire
     * @param e
     */
    handleChange(e) {
        this.setState({
            user: {
                ...this.state.user,
                [e.target.id]: e.target.value
            }
        })
    }

    /**
     * Appelé a la validation du formulaire
     * @param e
     * @returns {Promise<void>}
     */
    async submit(e) {
        e.preventDefault();
        let {id} = this.props.match.params;
        let {firstname, lastname, username, email,password, role} = this.state.user;

        let body = {
            firstname,
            lastname,
            username,
            email,
            role,
            password
        };

        console.log(body);


        let response = await UserService.create(body);

        let data = await response.json();

        if (response.ok) {
            console.log('created');
            this.props.history.push(`/admin/users/${data.user.id}`);
        }
    }

    render() {
        return (
            <div className="container">
                <h1>Création d'un Utilisateur</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input id="username" className="form-control" type="text"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input id="email" className="form-control" type="text"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input id="password" className="form-control" type="password"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="firstname">FirstName</label>
                        <input id="firstname" className="form-control" type="text"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastname">LastName</label>
                        <input id="lastname" className="form-control" type="text"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="role">Role</label>
                        <select className="form-control" name="role" id="role"
                                onChange={(e) => this.handleChange(e)} required>
                            <option value="STUDENT">Etudiant</option>
                            <option value="TEACHER">Professeur</option>
                            <option value="ADMIN">Admin</option>
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default CreateUser;
