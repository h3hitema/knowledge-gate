import React, {Component} from 'react';
import UserService from '../../../services/UserService';
import User from '../../../components/User';

class Users extends Component{

    state = {
        title: 'Le Titre',
        users: []
    }

    /**
     * Récupération de la liste des utilisateur
     * @returns {Promise<void>}
     */
    componentDidMount = async () => {
        let response = await UserService.list();
        if(response.ok){
            let data = await response.json();
            console.log(data);
            this.setState({users: data.users})
        }
    };

    /**
     * Boutton de suppression
     * @param id
     * @returns {Promise<void>}
     */
    async deleteUser(id) {
        console.log(id);
        let response = await UserService.delete(id);
        if(response.ok){
            let users = this.state.users;
            let index = users.findIndex(user => user.id === id);
            users.splice(index, 1);

            this.setState({users: users});
        }
    }

    render(){
        return(
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Mail</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.users.map(item => {
                                return (
                                    <User data={item} btntext="Supprimer" delete={(id) => this.deleteUser(id)}/>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Users;
