import React, {Component} from 'react';
import UserService from "../../../services/UserService";

class EditUser extends Component {

    state = {
        user: {
            firstname: '',
            lastname: '',
            email: '',
            role: '',
            username: '',
        }
    };

    /**
     * Récupération des infos de l'utilisateur
     * @returns {Promise<void>}
     */
    async componentDidMount() {
        let {id} = this.props.match.params;
        let response = await UserService.details(id);
        if (response.ok) {
            let data = await response.json();
            this.setState({user: data.user})
        }
    }

    /**
     * Mise a jours du state par rapport au formulaire
     * @param e
     */
    handleChange(e) {
        this.setState({
            user: {
                ...this.state.user,
                [e.target.id]: e.target.value
            }
        })
    }

    /**
     * Validation du formulaire
     * @param e
     * @returns {Promise<void>}
     */
    async submit(e) {
        e.preventDefault();
        let {id} = this.props.match.params;
        let {firstname, lastname, username, email, role} = this.state.user;

        let body = {
            firstname,
            lastname,
            username,
            email,
            role
        };

        let response = await UserService.update(id, body);

        if (response.ok) {
            console.log('updated')
            this.props.history.push(`/admin/users/${id}`);
        }
    }

    render() {
        let {firstname, lastname, username, email, role} = this.state.user;

        return (
            <div className="container">
                <h1>Détails du post...</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input id="username" className="form-control" type="text" value={username}
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input id="email" className="form-control" type="text" value={email}
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="firstname">FirstName</label>
                        <input id="firstname" className="form-control" type="text" value={firstname}
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastname">LastName</label>
                        <input id="lastname" className="form-control" type="text" value={lastname}
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="role">Role</label>
                        <select className="form-control" value={role} name="role" id="role"
                                onChange={(e) => this.handleChange(e)} required>
                            <option value="ADMIN">Admin</option>
                            <option value="TEACHER">Professeur</option>
                            <option value="STUDENT">Etudiant</option>
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default EditUser;
