import React, { Component } from 'react';
import {Link} from "react-router-dom";
import DisciplineService from "../../../services/DisciplineService";

class DetailsDiscipline extends Component {

    state = {
        discipline: {}
    };

    /**
     * Récupération de la discipline
     * @returns {Promise<void>}
     */
    componentDidMount = async () => {
        let { id } = this.props.match.params;
        let response = await DisciplineService.details(id);
        if (response.ok) {
            let data = await response.json();
            this.setState({discipline: data.discipline})
        }
    };

    /**
     * Supprimer une discipline par son id
     * @param id
     * @returns {Promise<void>}
     */
    async delete(id) {
        const response = await DisciplineService.delete(id);

        if(response.ok){
            this.props.history.push('/admin/disciplines');
        }
    }

    render() {
        let {id, name, slug, description} = this.state.discipline;
        return (
            <div className="container">
                <h1>Détails de la discipline...</h1>
                <p>Nom: {name}</p>
                <p>Email: {slug}</p>
                <p>Description: {description}</p>
                <p>
                    <button className="btn btn-danger" onClick={() => this.delete(id) }>Delete</button>
                </p>
                <p>
                    <Link className="btn btn-warning" to={`/admin/disciplines/edit/${id}`} >Editer</Link>
                </p>
            </div>
        )
    }
}

export default DetailsDiscipline;
