import React, {Component} from 'react';
import DisciplineService from "../../../services/DisciplineService";
import UserService from "../../../services/UserService";

class EditDiscipline extends Component {

    state = {
        discipline: {
            name: '',
            description: '',
            teachers: [],
            students: '',
        },
        teachers: [],
        selectedTeachersId: []
    };

    /**
     * Récupération de la Discipline et des prof de la discipline
     * @returns {Promise<void>}
     */
    async componentDidMount() {
        let {id} = this.props.match.params;
        // Detail de la discipline + profs associés
        let response = await DisciplineService.details(id);

        // Liste de tout les profs
        let response2 = await UserService.listTeachers();

        if (response.ok && response2.ok) {
            let data = await response.json();
            let data2 = await response2.json();
            let selectedTeachersId = data.discipline.teachers.map(teacher => {return teacher.id});
            this.setState({discipline: data.discipline,teachers:data2.teachers, selectedTeachersId: selectedTeachersId})
        }
    }

    /**
     * Mise a jour du state par rapport au formulaire
     * @param e
     */
    handleChange(e) {
        this.setState({
            discipline: {
                ...this.state.discipline,
                [e.target.name]: e.target.value
            },
        });
    }

    /**
     * Mise a jour du champ select du formulaire
     * @param e
     */
    handleSelectMultipleChange(e) {
        const options = e.target.options;
        let value = [];
        for (let i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }
        this.setState({selectedTeachersId: value});
    }

    /**
     * Edit la discipline lors de la validation du formulaire
     * @param e
     * @returns {Promise<void>}
     */
    async submit(e) {
        e.preventDefault();
        let {id} = this.props.match.params;
        let {name, description} = this.state.discipline;
        let selectedTeachersId = this.state.selectedTeachersId;
        let body = {
            name,
            description,
        };
        let response = await DisciplineService.update(id, body);
        let response2 = await DisciplineService.updateTeachers(id,selectedTeachersId);
        if (response.ok && response2.ok) {
            console.log('updated');
            this.props.history.push(`/admin/disciplines/${id}`);
        }
    }

    render() {
        let {name, description} = this.state.discipline;
        let selectedTeachersId = this.state.selectedTeachersId;
        console.log(selectedTeachersId);
        return (
            <div className="container">
                <h1>Editer la Discipline</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label htmlFor="name">Username</label>
                        <input name="name" className="form-control" type="text" value={name}
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea className="form-control" name="description"
                                  value={description}
                                  onChange={(e) => this.handleChange(e)} required/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="role">Role</label>
                        <select multiple={true} className="form-control" value={selectedTeachersId} name="teachersId" id="teachersId"
                                onChange={(e) => this.handleSelectMultipleChange(e)} required>
                            {
                                this.state.teachers.map((teacher) => {
                                    return (
                                        <option key={teacher.id} value={teacher.id}>
                                            {teacher.firstname} {teacher.lastname}
                                        </option>
                                    )
                                })
                            }

                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default EditDiscipline;
