import React, {Component} from 'react';
import UserService from "../../../services/UserService";
import DisciplineService from "../../../services/DisciplineService";

class CreateDiscipline extends Component {

    state = {
        discipline: {
            name: '',
            description:''
        }
    };

    async componentDidMount() {

    }

    /**
     * Récupération des donnés du formulaire
     * @param e
     */
    handleChange(e) {
        this.setState({
            discipline: {
                ...this.state.discipline,
                [e.target.id]: e.target.value
            }
        })
    }

    /**
     * Création de la discipline lors de la validation du formulaire
     * @param e
     * @returns {Promise<void>}
     */
    async submit(e) {
        e.preventDefault();
        let {name, description} = this.state.discipline;

        let body = {
            name,
            description
        };

        let response = await DisciplineService.create(body);
        console.log(response);
        let data = await response.json();

        if (response.ok) {
            console.log('created');
            this.props.history.push(`/admin/disciplines/${data.discipline.id}`);
        }
    }

    render() {
        return (
            <div className="container">
                <h1>Création d'une discipline</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label htmlFor="name">Nom</label>
                        <input id="name" className="form-control" type="text"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea className="form-control" id="description"
                                  onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default CreateDiscipline;
