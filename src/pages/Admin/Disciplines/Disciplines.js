import React, {Component} from 'react';
import UserService from '../../../services/UserService';
import Discipline from "../../../components/Discipline";
import DisciplineService from "../../../services/DisciplineService";

class Disciplines extends Component{

    state = {
        disciplines: []
    };

    /**
     * Récupération des disciplines
     * @returns {Promise<void>}
     */
    componentDidMount = async () => {
        let response = await DisciplineService.list();
        if(response.ok){
            let data = await response.json();
            console.log(data);
            this.setState({disciplines: data.disciplines})
        }
    };

    /**
     * Suppression d'une discipline => appeler par le composant <Discipline>
     * @param id
     * @returns {Promise<void>}
     */
    async delete(id) {
        let response = await DisciplineService.delete(id);
        if(response.ok){
            let data = await response.json();
            console.log(data);
            let disciplines = this.state.disciplines;
            let index = disciplines.findIndex(discipline => discipline.id === id);
            disciplines.splice(index, 1);

            this.setState({disciplines: disciplines});
        }
    }

    render(){
        return(
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Slug</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.disciplines.map(item => {
                                return (
                                    <Discipline  data={item} btntext="Supprimer" delete={(id) => this.delete(id)}/>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Disciplines;
