import React, {Component} from 'react';
import DisciplineService from "../../../services/DisciplineService";
import TeacherDiscipline from "../../../components/TeacherDiscipline";

class TeacherDisciplines extends Component {

    state = {
        disciplines: []
    };

    componentDidMount = async () => {
        const user = JSON.parse(localStorage.getItem('user'));
        let response = await DisciplineService.listTeacherDisciplines(user.id);
        if (response.ok) {
            let data = await response.json();
            this.setState({disciplines: data.disciplines})
        }
    };

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Slug</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.disciplines.map(item => {
                            return (
                                <TeacherDiscipline key={item.id} data={item}/>
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default TeacherDisciplines;
