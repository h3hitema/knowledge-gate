import React, {Component} from 'react';
import DisciplineService from "../../../services/DisciplineService";
import moment from "moment";
import {Calendar, momentLocalizer} from "react-big-calendar";

class TeacherShedules extends Component {

    state = {
        disciplines: [],
        localizer: momentLocalizer(moment),
        events: []
    };

    componentDidMount = async () => {
        const user = JSON.parse(localStorage.getItem('user'));
        let response = await DisciplineService.listTeacherDisciplines(user.id);
        if (response.ok) {
            let data = await response.json();
            this.setState({disciplines: data.disciplines})
        }

        /**
         * Calendar
         * @type {*[]}
         */
        let events= [];
        this.state.disciplines.map((discipline) => {
            discipline.classrooms.map(classroom => {
                events.push( {
                    title: `${discipline.name}`,
                    allDay: false,
                    start: new Date(classroom.dateStart),
                    end: new Date(classroom.dateENd),
                })
            })
        });
        console.log(events);
        moment.locale('fr-FR');
        const localizer = await momentLocalizer(moment);
        this.setState({localizer: localizer, events: events});
    };

    render() {
        return (
            <div className="container">
                <div style={{height:600+'px'}} className="calendar">
                    <Calendar
                        events={this.state.events}
                        localizer={this.state.localizer}
                        startAccessor={"start"}
                        endAccessor={"end"}
                    />
                </div>
            </div>
        )
    }
}

export default TeacherShedules;
