import React, { Component } from 'react';

class Register extends Component {

    render() {
        return (
            <div className="conteiner">
                <div className="form-group">
                    <label htmlFor="">Prénom</label>
                    <input id="firstname" className="form-control" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="">Nom</label>
                    <input id="lastname" className="form-control" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="">Email</label>
                    <input id="email" className="form-control" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="">Mot de passe</label>
                    <input type="password" id="passworm" className="form-control" required/>
                </div>
                <input className="btn btn-primary" type="submit"/>
            </div>
        )
    }
}
export default Register;