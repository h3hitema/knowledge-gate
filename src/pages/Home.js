import React, {Component} from 'react';
import PostService from '../services/PostService';
import Post from '../components/Post';

class Home extends Component{

    /*constructor(props) {
        super(props);
        this.state = {

        }
    }*/

    state = {
        title: 'Le Titre',
        posts: []
    };

    async componentDidMount() {
        let response = await PostService.list();
        if(response.ok){
            let data = await response.json();
            console.log(data);
            this.setState({posts: data.posts})
        }
    }

    async deletePost(id) {
        let response = await PostService.delete(id);
        if(response.ok){
            let posts = this.state.posts;
            let index = posts.findIndex(post => post._id === id);
            posts.splice(index, 1);

            this.setState({posts: posts});
        }
    }

    render(){
        return(
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titre</th>
                            <th>Contenu</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.posts.map(item => {
                                return (
                                    <Post data={item} btntext="Supprimer" delete={(id) => this.deletePost(id)}/>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Home;
