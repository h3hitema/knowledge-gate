import React, {Component} from 'react';
import {Link} from 'react-router-dom'

class Header extends Component {
    state = {
        token: "",
        user: {}
    };

    componentDidMount() {
        this.setState({
            token: localStorage.getItem('token'),
            user: JSON.parse(localStorage.getItem('user'))
        });
    }

    disconnect() {
        localStorage.clear();
        this.setState({isAuth: false});
    }

    renderHeaderNav() {
        const {role} = this.state.user;
        switch (role) {
            case 'ADMIN':
                return (
                    [
                        <li key="1" className="nav-item">
                            <Link className="nav-link" to={'/admin/users'}>Utilisateurs</Link>
                        </li>,
                        <li key="2" className="nav-item">
                            <Link className="nav-link" to={'/admin/disciplines'}>Disciplines</Link>
                        </li>
                    ]
                );
            case 'TEACHER':
                return (
                    [
                        <li key="1" className="nav-item">
                            <Link className="nav-link" to={'/teacher/disciplines'}>Mes Disciplines</Link>
                        </li>,
                        <li key="2" className="nav-item">
                            <Link className="nav-link" to={'/teacher/schedule'}>Emplois du temps</Link>
                        </li>
                    ]
                );
            case 'STUDENT':
                return (
                    [
                        <li key="1" className="nav-item">
                            <Link className="nav-link" to={'/student/disciplines'}>Les Disciplines</Link>
                        </li>,
                    ]
                );
            default:
                return 'foo';
        }
    }

    render() {
        let {token} = this.state;

        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" to={'/'}>Navbar</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    {token ? (
                        <ul className="navbar-nav mr-auto">
                            {
                                this.renderHeaderNav()
                            }
                            <li key="3" className="nav-item">
                                <Link className="nav-link" onClick={(e) => this.disconnect()}
                                      to={'/login'}>Deconnexion</Link>
                            </li>
                        </ul>
                    ) : (
                        <ul className="navbar-nav mr-auto">
                            <li key="1" className="nav-item">
                                <Link className="nav-link" to={'/login'}>Connexion</Link>
                            </li>
                            <li key="2" className="nav-item">
                                <Link className="nav-link" to={'/register'}>Inscription</Link>
                            </li>
                        </ul>
                    )}
                </div>
            </nav>
        )
    }
}

export default Header;