import React, {Component} from 'react';
import DisciplineService from "../services/DisciplineService";
import moment from "moment";

class CreateClassRoom extends Component {

    state = {
        id: '',
        date: '',
        timeStart: '',
        timeEnd: '',
    };

    componentDidMount = async () => {
        console.log(this.props);
        let {id} = this.props;

        this.setState({id:id});
    };


    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
        console.log(this.state);
    }

    async submit(e) {
        e.preventDefault();
        let {date, timeStart, timeEnd} = this.state;

        let dateStart = new Date(`${date} ${timeStart}`).getTime()/1000;
        let dateEnd = new Date(`${date} ${timeEnd}`).getTime()/1000;

        let body = {
            dateStart,
            dateEnd
        };

        let response = await DisciplineService.createClassRoom(this.props.id,body);

        if (response.ok) {
            console.log('created');
            window.location.reload();
        }
    }

    render() {
        return (
            <div className="container mb-5">
                <h1>Création d'un cours</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label htmlFor="date">Date du cours</label>
                        <input id="date" className="form-control" type="date"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="timeStart">Heure de début</label>
                        <input className="form-control" id="timeStart" type="time"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="timeEnd">Heure de fin</label>
                        <input className="form-control" id="timeEnd" type="time"
                               onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default CreateClassRoom;
