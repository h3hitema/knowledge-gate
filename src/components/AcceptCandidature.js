import React, {Component} from 'react';
import DisciplineService from "../services/DisciplineService";

class AcceptCandidature extends Component {

    state = {
        id: '',
        student: {},
        application: {}
    };

    componentDidMount = async () => {
        let {id, student, application} = this.props;

        this.setState({id: id, student:student, application: application});
    };


    async submit(e) {
        e.preventDefault();

        let response = await DisciplineService.acceptCandidature(this.state.id);

        if (response.ok) {
            console.log('created');
            window.location.reload();
        }
    }

    render() {
        const {student, application} = this.state;
        return (
            <div className="container mb-5">
                <form onSubmit={(e) => this.submit(e)}>
                    <ul>
                        <li>Nom: {student.firstname} {student.lastname}</li>
                        <li>Message: {application.message}</li>
                        <li><button type="submit" className="btn btn-primary">Accepter</button></li>
                    </ul>
                </form>
            </div>
        )
    }
}

export default AcceptCandidature;
