import React, {Component} from 'react';
import {Link} from "react-router-dom";

class StudentDiscipline extends Component {

    // constructor(props) {
    //     super(props);
    // }

    componentDidMount() {
        //console.log(this.props.data)
    }

    render() {
        let {id, name, slug, description} = this.props.data;

        return(
            <tr>
                <td>{id}</td>
                <td>{name}</td>
                <td>{slug}</td>
                <td>{description}</td>
                <td>
                    <Link className="btn btn-primary" to={`/student/disciplines/${id}`} >Voir</Link>
                </td>
            </tr>
        )
    }
}

export default StudentDiscipline;