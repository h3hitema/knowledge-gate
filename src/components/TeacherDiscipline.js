import React, {Component} from 'react';
import {Link} from "react-router-dom";

class TeacherDiscipline extends Component {

    // constructor(props) {
    //     super(props);
    // }

    componentDidMount() {
    }

    render() {
        let {id, name, slug, description} = this.props.data;

        return(
            <tr>
                <td>{id}</td>
                <td>{name}</td>
                <td>{slug}</td>
                <td>{description}</td>
                <td>
                    <Link className="btn btn-primary" to={`/teacher/disciplines/${id}`} >Voir</Link>
                </td>
            </tr>
        )
    }
}

export default TeacherDiscipline;