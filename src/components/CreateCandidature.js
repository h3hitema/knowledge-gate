import React, {Component} from 'react';
import DisciplineService from "../services/DisciplineService";

class CreateCandidature extends Component {

    state = {
        message: ''
    };

    componentDidMount = async () => {
        let {id} = this.props;

        this.setState({id: id});
    };


    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
        console.log(this.state);
    }

    async submit(e) {
        e.preventDefault();
        const student = JSON.parse(localStorage.getItem('user'))
        console.log(student);

        const body = {
            message: this.state.message,
            student_id: student.id
        };

        let response = await DisciplineService.createCandidature(this.props.id, body);

        if (response.ok) {
            console.log('created');
            window.location.reload();
        }
    }

    render() {
        return (
            <div className="container mb-5">
                <h1>S'inscrire au cours</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label htmlFor="message">Motivation</label>
                        <textarea className="form-control" id="message" minLength={20}
                                  onChange={(e) => this.handleChange(e)} required/>
                    </div>
                    <button type="submit" className="btn btn-primary">S'inscrire</button>
                </form>
            </div>
        )
    }
}

export default CreateCandidature;
