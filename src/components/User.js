import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Users extends Component {

    // constructor(props) {
    //     super(props);
    // }

    componentDidMount() {
        console.log(this.props.data)
    }

    render() {
        let {id, firstname, lastname, email, role} = this.props.data;

        return(
            <tr>
                <td>{id}</td>
                <td>{firstname} {lastname}</td>
                <td>{email}</td>
                <td>{role}</td>
                <td>
                    <button className="btn btn-danger" onClick={() => this.props.delete(id) }>{this.props.btntext}</button>
                    <Link className="btn btn-primary" to={`/admin/users/${id}`} >Voir</Link>
                    <Link className="btn btn-warning" to={`/admin/users/edit/${id}`} >Editer</Link>
                </td>
            </tr>
        )
    }
}

export default Users;