import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Discipline extends Component {

    // constructor(props) {
    //     super(props);
    // }

    componentDidMount() {
        //console.log(this.props.data)
    }

    render() {
        let {id, name, slug, description} = this.props.data;

        return(
            <tr>
                <td>{id}</td>
                <td>{name}</td>
                <td>{slug}</td>
                <td>{description}</td>
                <td>
                    {
                        this.props.delete ?
                            <button className="btn btn-danger" onClick={() => this.props.delete(id) }>{this.props.btntext}</button>
                            : ""
                    }
                    <Link className="btn btn-primary" to={`/admin/disciplines/${id}`} >Voir</Link>
                    <Link className="btn btn-warning" to={`/admin/disciplines/edit/${id}`} >Editer</Link>
                </td>
            </tr>
        )
    }
}

export default Discipline;