const baseUrl = "http://localhost:5000";

class UserService {
    /**
     * Liste de tout les utilisateur
     * @returns {Promise<Response>}
     */
    static async list() {
        const token = localStorage.getItem('token');
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };

        return await fetch(`${baseUrl}/admin/user`, init);
    }

    /**
     * Liste de tout les profs
     * @returns {Promise<Response>}
     */
    static async listTeachers() {
        const token = localStorage.getItem('token');
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };
        return await fetch(`${baseUrl}/admin/teacher`, init);
    }

    /**
     * Détail d'un utilisateur
     * @param id
     * @returns {Promise<Response>}
     */
    static async details(id) {
        const token = localStorage.getItem('token');
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };

        return await fetch(`${baseUrl}/admin/user/${id}`, init);
    }

    /**
     * Edition d'un utilisateur
     * @param id
     * @param body
     * @returns {Promise<Response>}
     */
    static async update(id, body)
    {
        const token = localStorage.getItem('token');
        let init = {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(body)
        };
        console.log(init);
        return await fetch(`${baseUrl}/admin/user/${id}`, init);
    }

    /**
     * Création d'un utilisateur
     * @param body
     * @returns {Promise<Response>}
     */
    static async create(body)
    {
        const token = localStorage.getItem('token');
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(body)
        };
        console.log(init);
        return await fetch(`${baseUrl}/admin/user/`, init);
    }


    /**
     * Suppression d'un utilisateur
     * @param id
     * @returns {Promise<Response>}
     */
    static async delete(id) {
        const token = localStorage.getItem('token');
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };
        return await fetch(`${baseUrl}/admin/user/${id}`, init);
    }

    /**
     * Authentification d'un utilisateur
     * @param body
     * @returns {Promise<Response>}
     */
    static async auth(body) {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",

            },
            body: JSON.stringify(body)
        };
        return await fetch(`${baseUrl}/auth/login`, init);
    }
}


export default UserService;