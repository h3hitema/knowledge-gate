const baseUrl = "http://localhost:5000";

class DisciplineService {
    /**
     * Liste de toute les disciplines
     * @returns {Promise<Response|null>}
     */
    static async list() {
        const token = localStorage.getItem('token');
        const user = JSON.parse(localStorage.getItem('user'));

        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };

        switch (user.role) {
            case "ADMIN":
                return await fetch(`${baseUrl}/admin/discipline`, init);
            case "STUDENT":
                return await fetch(`${baseUrl}/student/discipline`, init);
            default:
                return null;
        }


    }

    /**
     * Liste des Discipline d'un prof
     * @param id
     * @returns {Promise<Response>}
     */
    static async listTeacherDisciplines(id) {
        const token = localStorage.getItem('token');
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
        };
        return await fetch(`${baseUrl}/teacher/${id}/disciplines`, init);
    }

    /**
     * Detail d'une discipline
     * @param id
     * @returns {Promise<Response>}
     */
    static async details(id) {
        const token = localStorage.getItem('token');
        const user = JSON.parse(localStorage.getItem('user'));

        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };
        switch (user.role) {
            case "ADMIN":
                return await fetch(`${baseUrl}/admin/discipline/${id}`, init);
            case "TEACHER":
                return await fetch(`${baseUrl}/teacher/discipline/${id}`, init);
            case "STUDENT":
                return await fetch(`${baseUrl}/student/discipline/${id}`, init);
            default:
                break;
        }
    }

    /**
     * Permet de d'editer une discipline
     * @param id
     * @param body
     * @returns {Promise<Response>}
     */
    static async update(id, body) {
        const token = localStorage.getItem('token');
        let init = {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(body)
        };
        console.log(init);
        return await fetch(`${baseUrl}/admin/discipline/${id}`, init);
    }

    /**
     * Permet de selectionner les profs d'une discipline
     * @param id
     * @param teachersId
     * @returns {Promise<Response>}
     */
    static async updateTeachers(id, teachersId) {
        const token = localStorage.getItem('token');
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify({teachersId})
        };
        console.log(init);
        return await fetch(`${baseUrl}/admin/discipline/${id}/teachers`, init);
    }

    /**
     * Permet de créer une discipline
     * @param body
     * @returns {Promise<Response>}
     */
    static async create(body) {
        console.log(body);
        const token = localStorage.getItem('token');
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(body)
        };
        return await fetch(`${baseUrl}/admin/discipline/`, init);
    }


    /**
     * Permet de supprimer une discipline
     * @param id
     * @returns {Promise<Response>}
     */
    static async delete(id) {
        const token = localStorage.getItem('token');
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        };
        return await fetch(`${baseUrl}/admin/discipline/${id}`, init);
    }

    /**
     * Permet de créer un cour lié à une discipline
     * @param id
     * @param body
     * @returns {Promise<Response>}
     */
    static async createClassRoom(id, body) {
        const token = localStorage.getItem('token');
        console.log(body);
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify(body)
        };
        return await fetch(`${baseUrl}/teacher/discipline/${id}/classroom`, init);
    }

    /**
     * Permet à un étudiant de postuler à une discipline
     * @param id
     * @param body
     * @returns {Promise<Response>}
     */
    static async createCandidature(id, body) {
        const token = localStorage.getItem('token');
        console.log(id);
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify(body)
        };
        return await fetch(`${baseUrl}/student/discipline/${id}/apply`, init);
    }

    /**
     * Permet au profs d'accepter un élève
     * @param id
     * @returns {Promise<Response>}
     */
    static async acceptCandidature(id) {
        const token = localStorage.getItem('token');
        console.log(id);
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
        };
        return await fetch(`${baseUrl}/teacher/discipline/apply/accept/${id}`, init);
    }
}


export default DisciplineService;