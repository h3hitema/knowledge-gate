const baseUrl = "http://localhost:5000";

class PostService {
    static async list()
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }
        let call = await fetch(`${baseUrl}/posts`, init);
        return call;
    } 

    static async details(id)
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };
        return await fetch(`${baseUrl}/posts/${id}`, init);
    } 

    static async update(id, body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        }
        console.log(init);
        let call = await fetch(`${baseUrl}/posts/${id}`, init);
        return call;
    } 

    static async delete(id)
    {
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${baseUrl}/posts/${id}`, init);
        return call;
    } 
}


export default PostService;